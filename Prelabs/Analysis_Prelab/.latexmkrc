$pdf_mode = 1;

@default_files = ("problems.tex");

# Do not run a previewer
$preview_mode = 0;

# Do not watch for updating files
$preview_continuous_mode = 0;
