Ongoing work for the Davis Advanced Lab (122). Folders correspond to
individual elements of the course. What follows is a current state

1.	   Statistical Introduction - nonexistant

	   Davis physics offers no exposure to statistical ideas. This
	   immediately becomes a problem in experimental
	   classes. Advanced lab currently recommends reading
	   regarding statistical analysis, but without an introduction
	   to the ideas of error propigation, the central limit
	   theorem, degrees of freedom, and confidance testing,
	   students are frequently lost.

	   There is good overlap with the data analysis portion of
	   this course. It might be smart to present these ideas
	   coupled with monty carlo 'experiments' to test them.

2.  	   [Python Introduction](IPython_Introduction/ClassMaterials/Python-FirstSteps.pdf)

	   Practicing physiscists, and increasingly everyone else,
	   need to have basic programming skills. To ensure that all
	   students gr aduate with some exposure to this, Advanced lab
	   is moving towards solving the practical problems of data
	   management and analysis in IPython. As incoming students
	   have a range of computational abilities and interests, the
	   text attempts to be an essential introduction for novices,
	   a reference for those learning, and unobtrusive to the
	   skilled.

	   As more students use the text and questions, they should be
	   refined and clarified. Other goals include an example
	   analysis, better code typeography, interactive problems,
	   and a possible conversion to ConTeXt.

4. 	   Success Tracking -nonexistant

	   In both the prelabs and the labs, students are making
	   similar mistakes across classes. This deserves attention -
	   the first step is to start comparing success. A database of
	   prelabs and scores would be a good place to start.

5. 	   Prelabs -nascent

	   There are currently 14 prelabs in total. They should be
	   moved to a clean format that can be printed and viewed
	   online. Solutions should also be made available.

	   The current plan is to have questions stored individually
	   in .tex fragments, and automatically converted to html
	   using katex with pandoc. This will allow interactive
	   solutions where appropriate, probably using IPython notebooks.

